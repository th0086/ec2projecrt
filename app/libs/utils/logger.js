module.exports = {
  useConsoleLog: true,
  log: function(message) {
    if (this.useConsoleLog) {
      console.log(message);
    }
  },
  error: function(errorMessage) {
    if (this.useConsoleLog) {
      console.error(errorMessage);
    }
  },
};
