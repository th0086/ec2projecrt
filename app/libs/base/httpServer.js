let bodyParser = require('body-parser');
let cookieParser = require('cookie-parser');
let cors = require('cors');
let express = require('express');
require('express-async-errors');
let fs = require('fs');
let https = require('https');
let path = require('path');
let logger = require('../utils/logger');
let ignoreRouteHandler = require('../middlewares/ignoreRouteHandler');
let errorHandler = require('../middlewares/errorHandler');
let recordHandler = require('../middlewares/resHandler');

let app = express();
app.use(cors());
app.use(bodyParser.json({limit: '1024mb'}));// 设置post body数据的大小
app.use(bodyParser.urlencoded({limit: '1024mb', extended: true}));
app.use(cookieParser());
app.use(ignoreRouteHandler);
app.use(recordHandler);

module.exports.app = app;
// 创建http服务器
module.exports.createHttpServer = function(port) {
  errorHandler.initErrorHandler(app);
  app.set('port', port);
  let server = app.listen(app.get('port'), function() {
    logger.log('http服务器启动成功,端口号: ' + server.address().port);
  });
  return server;
};
// 创建https服务器
module.exports.createHttpsServer = function(port, keyPath, crtPath) {
  errorHandler.initErrorHandler(app);

  let privateKey = fs.readFileSync(path.resolve(__dirname, keyPath), 'utf8');
  let certificate = fs.readFileSync(path.resolve(__dirname, crtPath), 'utf8');
  let httpsServer = https.createServer({key: privateKey, cert: certificate}, app);
  httpsServer.listen(port, function() {
    logger.log('https服务器启动成功,端口号: ' + port);
  });
  return httpsServer;
};
