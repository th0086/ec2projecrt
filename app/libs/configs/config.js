const fs = require("fs");

module.exports = {
  httpServer: {
    sessionSecret: 'hello world',
    isHttps: false,
    httpPort: 9200,
    privateKeyPath: '',
    certificatePath: '',
  },
  sshConf: {
    host: 'ec2-3-17-140-125.us-east-2.compute.amazonaws.com', //EC2雲端主機位址(若重開機將會更動)
    port: 22,
    username: 'ubuntu',
    privateKey: fs.readFileSync("../app/ssh_key/Alan-fristEC2.pem"), //讀取私鑰
  },
  sqlConf: {
    host: '127.0.0.1',
    port: 3306,
    user: 'root',
    password: '123456',
    database: 'EC2TestDB',
  },
};
