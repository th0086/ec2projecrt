let mysqlClient = require('mysql2');
let SSH2Client = require('ssh2').Client;
let config = require('../configs/config');

let Connect = {
    getQuery: async function (sql, params) {

        const forwardConfig = {
            srcHost: '127.0.0.1', // 任何有效地址
            srcPort: 3306, // 任何有效端口
            dstHost: config.sqlConf.host, // 目標數據庫
            dstPort: config.sqlConf.port // 目標端口
        };

// 創建一個 SSH Client 實例
        const sshClient = new SSH2Client();

        //開啟SSH通道
        return await new Promise((resolve, reject) => {
            sshClient.on('ready', () => {
                sshClient.forwardOut(
                    forwardConfig.srcHost,
                    forwardConfig.srcPort,
                    forwardConfig.dstHost,
                    forwardConfig.dstPort,
                    (err, stream) => {
                        if (err) reject(err);

                        // 創建一個新的數據庫服務器對象
                        const updatedDbServer = {
                            ...config.sqlConf,
                            stream
                        };

                        // 連接到 mysql
                        const connection = mysqlClient.createConnection(updatedDbServer);

                        // 執行DB語法
                         connection.query(sql, params, function(error, results, fields) {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(results[0]);
                            }
                        });

                    });
            }).connect(config.sshConf);
        });
    },
}

module.exports = Connect;