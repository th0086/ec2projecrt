let statusCode = require('http-status');
let _ = require('lodash');
const lists = ['favicon.ico'];
module.exports = function(req, res, next) {
  let r = _.findIndex(lists, function(white) {
    return req.originalUrl.includes(white);
  });
  if (r > -1) {
    res.status(statusCode.NO_CONTENT).end();
    return;
  } else {
    next();
  }
};
