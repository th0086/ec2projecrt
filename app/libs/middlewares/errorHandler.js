const logger = require('../utils/logger');
const statusCode = require('http-status');

module.exports.initErrorHandler = function(app) {
  app.use(notFoundHandler);
  app.use(errorHandler);
};

let errorHandler = function(err, req, res, next) {
  logger.error(req.ip);
  logger.error(`请求出错 ${req.method} : ${req.url}`);
  if ((err.status || 500) === 500) {
    logger.error(' Caught exception: ' + err.stack);
  } else {
    logger.error(err.message);
  }
  res.status(err.status || 500).end(err.message);
};

let notFoundHandler = function(req, res, next) {
  let err = new Error('Not Found');
  err.status = statusCode.NOT_FOUND;
  next(err);
};
