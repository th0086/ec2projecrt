const logLevel = require('../enums/logLevel');
const logger = require('../utils/logger');
const _ = require('lodash');
module.exports = function(req, res, next) {
  let oriEnd = res.end;
  res.end = function(body) {
    const params = {
      level: this.statusCode >= 400 ? (this.statusCode >= 500 ? logLevel.Error : logLevel.Warning) : logLevel.Info,
      method: this.req.method,
      url: this.req.originalUrl.substring(0, this.req.originalUrl.indexOf('?') > -1 ? this.req.originalUrl.indexOf('?') : this.req.originalUrl.length),
      clientIp: req.ip.replace(/::ffff:/g, ''),
      userName: req.user?.name,
      body: this.req.body,
      params: this.req.params,
      query: this.req.query,
      resStatus: this.statusCode,
      resBody: this.statusCode !== 200 ? (body || '') : '请求成功',
    };
    let requestParams = {};
    _.assign(requestParams, this.req.body, this.req.query, this.req.params);

    oriEnd.call(this, body);
  };
  next();
};
