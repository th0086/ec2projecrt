module.exports = Object.freeze({
  Debug: 'DEBUG',
  Info: 'INFO',
  Warning: 'WARNING',
  Error: 'ERROR',
  Critical: 'CRITICAL',
});
