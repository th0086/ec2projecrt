const mysqldb = require('../libs/db/dbHelper');

let UserRepo = {
    getUserAsync: async function(Name) {
        const sql = 'select * from EC2TestDB.User where Name = ?;';
        const param = [Name];
        let data = [];
        try {
            await mysqldb.getQuery(sql, param).then(result => {
                data.push(result)
            });
            return data;
        } catch (error) {
            throw error;
        }
    },
};

module.exports = UserRepo;