let express = require('express');
let srv = require('../../app/services/UserSrv');
const camelcaseKey = require('camelcase-keys'); //統整駝峰功能
let router = express.Router();

// 獲取User資訊
router.get('/',  async function(req, res) {

    try {
        let result = await srv.getUserAsync(req.query.Name);
        res.json(camelcaseKey(result, {deep: true})).end();
    } catch (error) {
        console.log(error);
    }

});

module.exports = router;