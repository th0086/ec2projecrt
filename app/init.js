let path = require('path');
let httpServer = require('./libs/base/httpServer');

module.exports.init = function(config, callback) {
    // 配置session
    httpServer.app.use('/User', require('./routes/UserController'));
    if (!config.isHttps) {
        return httpServer.createHttpServer(config.httpPort);
    } else {
        return httpServer.createHttpsServer(config.httpPort, path.resolve(__dirname, config.privateKey), path.resolve(__dirname, config.certificatePath));
    }
};
