
// 出错是打印出错信息
let logger = require('./libs/utils/logger');
let config = require('./libs/configs/config');
let init = require('./init');

process.on('uncaughtException', function(err) {
    logger.error(' Caught exception: ' + err.stack);
});
// 捕获async异常
process.on('unhandledRejection', (reason, p) => {
    logger.error('Caught Unhandled Rejection at:' + p + 'reason:' + reason.stack);
});

if (config?.httpServer?.httpPort) {
    init.init(config.httpServer, function() {
    });
} else {
    logger.error('请输入服务器port');
}
