let repo = require('../../app/repositories/UserRepo');

let UserSrv = {
    getUserAsync: async function(Name) {
        try {
            const result = await repo.getUserAsync(Name);
            return {
                rows: result[0]
            };
        }catch (error) {
            throw error;
        }
    },
};

module.exports = UserSrv;
